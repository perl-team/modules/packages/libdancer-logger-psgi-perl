libdancer-logger-psgi-perl (1.0.1-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 22:46:10 +0100

libdancer-logger-psgi-perl (1.0.1-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 14:11:03 +0100

libdancer-logger-psgi-perl (1.0.1-2) unstable; urgency=medium

  * Relax to build-depend unversioned on libmodule-build-perl: Needed
    version satisfied even in oldstable.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 13:18:34 +0200

libdancer-logger-psgi-perl (1.0.1-1) unstable; urgency=low

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Fix use canonical host (anonscm.debian.org) in Vcs-Git URI.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Bump to standards-version 3.9.5.
  * Fix use canonical Vcs-Git URL.
  * Update homepage.
  * Add git URL as alternate source.
  * Update watch file to use metacpan.org URL.
  * Update copyright info:
    + Extend coverage of packaging, and bump its licensing to GPL-3+.
    + Fix use comment and license pseudo-sections to obey silly
      restrictions of copyright format 1.0.
    + Change author, and relicense to Expat.
    + Update preferred upstream contact.
  * Bump debhelper compatibility level to 8: debhelper 8 is available
    even in oldstable.
  * Update package relations:
    + Fix build-depend explicitly on perl.
    + Fix merge duplicate build-dependencies.
    + Relax to build-depend unversioned on cdbs: Needed version
      satisfied even in oldstable.
  * Stop track tarball checksum: Check against upstrem git instead.
  * Fix get-orig-source hints (add version prefix).

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 20 May 2014 12:17:40 +0200

libdancer-logger-psgi-perl (0.04-2) unstable; urgency=low

  * Fix build-depend on recent perl, or alternatively a recent
    libmodule-build-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 May 2012 02:16:24 +0200

libdancer-logger-psgi-perl (0.04-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#671797.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 May 2012 01:45:48 +0200
